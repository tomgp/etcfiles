#!/bin/bash
set -euo pipefail

extract_kernels() {
  local _bootdir=/boot
  _kernels=()
  for i in "$_bootdir"/vmlinuz-*; do
    _kernels+=("$(basename "$i" | cut -c 9-)")
  done
}

extract_kernels

_linux_targets=()
_nvidia_targets=()
while read -r target; do 
  for splitted in $target; do
    case $splitted in
      linux*)
        [[ "$splitted" == *-docs || "$splitted" == *-headers ]] || _linux_targets+=("$splitted");;
      nvidia-dkms)
        _nvidia_targets+=("$splitted");;
      nvidia)
        _nvidia_targets+=("$splitted");;
    esac
  done
done

[[ ${#_nvidia_targets[@]} == 0 ]] && exit 0

for kernel in "${_kernels[@]}"
do
	[[ " ${_linux_targets[*]} " =~ " $kernel " ]] || /usr/bin/mkinitcpio -P "$kernel"
done
