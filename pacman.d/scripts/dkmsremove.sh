#!/bin/bash
set -euo pipefail

kernel_versions() {
	local _bootdir=/boot
	kernel_vers=()
	for i in "$_bootdir"/vmlinuz-*; do
		kernel_vers+=("$(file "$i" | cut -d ',' -f 2 | cut -d ' ' -f 3)")
	done
}

obtain_what_to_remove() {
	local _module
	local _kernver
	what_to_remove=()
	while read -r line; do
		_module=$(echo "$line" | cut -d ':' -f 1 | cut -d ',' -f 1 | tr -d ' ')	
		_kernver=$(echo "$line" | cut -d ':' -f 1 | cut -d ',' -f 2 | tr -d ' ')
		[[ ! " ${kernel_vers[*]} " =~ " ${_kernver} " ]] && what_to_remove+=( "${_module};${_kernver}" )
	done < <( dkms status ) || true
}

kernel_versions
obtain_what_to_remove

echo
echo "Detected installed kernel versions:"
for i in ${kernel_vers[@]}; do
	echo "   $i"
done

if [ ${#what_to_remove[@]} == 0 ]; then
	echo
	echo "No old DKMS modules to be removed..."
	echo
	exit 0
fi

echo
echo "Removing old DKMS modules:"
for i in ${what_to_remove[@]}; do
	_module=$(echo $i | cut -d ';' -f 1)
	_kernver=$(echo $i | cut -d ';' -f 2)
	dkms remove $_module -k $_kernver
done

echo
