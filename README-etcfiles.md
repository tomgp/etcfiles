# Etc files

## /etc/locale.conf

```
/usr/bin/localectl --no-convert set-keymap es
```

```
/usr/bin/localctl set-locale LANG=en_IE.UTF-8 
```

## /etc/X11/xorg.conf.d/00-keyboard.conf

```
/usr/bin/localectl --no-convert set-x11-keymap es pc105 '' 'numpad:microsoft, caps:hyper'
```
